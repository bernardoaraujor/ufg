# ufg

Repositório da Dissertação de Mestrado em Engenharia de Computação de Bernardo Araujo Rodrigues para o Programa de Pós Graduação da Escola de Engenharia Elétrica, Mecânica e de Computação (EMC) da Universidade Federal de Goiás (UFG).

A Dissertação recebe o título de &#34;CORINDA: QUEBRANDO HASHES DE SENHAS
CONCORRENTEMENTE COM A LINGUAGEM DE
PROGRAMAÇÃO GO&#34;.

O código fonte do software Corinda pode ser encontrado em https://github.com/bernardoaraujor/corinda .