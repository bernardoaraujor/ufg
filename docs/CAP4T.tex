%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CAP\'{i}TULO 4
\chapter{METODOLOGIA}\label{chapter:cap4}

\par Neste cap\'{i}tulo \'{e} apresentada a metodologia para o desenvolvimento do software denominado \textbf{Corinda}, que \'{e} um quebrador de senhas. As estrat\'{e}gias de gera\c{c}\~{a}o de palpites de senhas s\~{a}o baseadas nos conceitos de modelos, concorr\^{e}ncia e entropia. Os \textit{hashes} alvo s\~{a}o previamente conhecidos, o que faz do \textbf{Corinda} o quebrador \textit{offline}, implementado na linguagem de programa\c{c}\~{a}o \textbf{Go}. Ainda \'{e} realizado neste cap\'{i}tulo a descri\c{c}\~{a}o dos experimentos para validar o desempenho do \textbf{Corinda}.

\section{Modelagem do Corinda}

\par Com o intuito de construir o software para quebras de senhas com processos sequenciais comunicantes em unidades de processamento central (CPU), desenvolve-se o \textbf{Corinda}. A metodologia implementada para o desenvolvimento do \textbf{Corinda} parte da investiga\c{c}\~{a}o dos padr\~{o}es estat\'{i}sticos dos modelos encontrados em conjuntos amostrais de senhas, da investiga\c{c}\~{a}o dos par\^{a}metros entropia e frequ\^{e}ncia relativa dos modelos encontrados nestes conjuntos amostrais e na utiliza\c{c}\~{a}o da entropia e da frequ\^{e}ncia relativa para a cria\c{c}\~{a}o de heur\'{i}stica concorrente para quebra das senhas. O escopo deste trabalho n\~{a}o inclui o conceito de \textit{salting}, pr\'{a}tica comum para dificultar a quebra de \textit{hashes}. De forma a incorporar a opera\c{c}\~{a}o de parti\c{c}\~{a}o de \textit{strings}, a metodologia proposta expande o conceito de modelo apresentado na se\c{c}\~{a}o \ref{sec:model}, para a forma de modelo elementar e modelo composto.

\subsection{Modelo Elementar e Modelo Composto}

\par A defini\c{c}\~{a}o de modelo elementar e de modelo composto consiste na adapta\c{c}\~{a}o estabelecida para os fins deste trabalho e n\~{a}o constam na literatura cl\'{a}ssica da teoria dos modelos de primeira ordem \cite{chang_keisler_2012}. No desenvolvimento do \textbf{Corinda}, utiliza-se o termo modelo elementar para representar as subdivis\~{o}es at\^{o}micas nas parti\c{c}\~{o}es da estrutura da \textit{string}.

\par O modelo composto \'{e} definido como, seja $\Xi$ o conjunto bem ordenado de modelos elementares, dado por:

\begin{equation}
 \Xi = \{m_{i=1}, \cdots, m_N\}
\end{equation}

\par Define-se o \textbf{modelo composto} $\tilde m$ como o modelo cujo dom\'{i}nio $\tilde \lambda$ \'{e} formado pelo produto cartesiano dos dom\'{i}nios $\lambda_i$ dos modelos simples $m_i \in \Xi$, dado por:

\begin{equation}
\tilde \lambda = \lambda_{i=1} \times \cdots \times \lambda_N
\end{equation}

\par Por exemplo, seja $\Xi = \{m_a, m_b\}$, seja $\lambda_a$ o dicion\'{a}rio de nomes de cidad\~{a}os brasileiros, e $\lambda_b$ o conjunto de \textit{strings} composta por n\'{u}meros em formato de data, ent\~{a}o:

\begin{equation}
 \alpha_a(s) \implies s \in \lambda_a
\end{equation}

\begin{equation}
 \phi_a(s): \alpha_a(s) \implies ``bernardo" \in \lambda_a
\end{equation}

\begin{equation}
 \alpha_b(s) \implies s \in \lambda_b
\end{equation}

\begin{equation}
 \phi_b(s): \alpha_b(s) \implies ``310790" \in \lambda_b
\end{equation}

\hfill \break
\noindent portanto:
\hfill \break

\begin{equation}
 \tilde \lambda = \lambda_a \times \lambda_b
\end{equation}

\begin{equation}
 \tilde s = \Psi^{-1}(s_a\in \lambda_a|s_b\in \lambda_b)
\end{equation}


\begin{equation}
 \tilde \alpha(\tilde s) \implies \tilde s \in \tilde \lambda
\end{equation}

\begin{equation}
 \tilde \phi(\tilde s): \tilde \alpha (\tilde s)
\end{equation}

\begin{equation}\label{eq30}
 \tilde m \vDash \tilde \phi(s) \implies ``bernardo310790" \in \tilde\lambda
\end{equation}

\par Observa-se que a cardinalidade do modelo composto \'{e} definida pelo produto das cardinalidades de cada $m_i \in \Xi$, dado por:

\begin{equation}
\displaystyle\mathcal{C}(\tilde m) = \prod_{i=1}^{N} \mathcal{C}(m_i)
\end{equation}

\par Assim, se $\mathcal{C}(m_a) = 30.000$ e $\mathcal{C}(m_b) = 930.000$, ent\~{a}o $\mathcal{C}(\tilde m) = 27.900.000.000$.

\subsection{\textit{Token}}

\par Na composi\c{c}\~{a}o estrutural do modelo composto, refere-se como \textbf{\textit{token}} a \textit{substring} correspondente a determinado modelo elementar. Desta forma, a \textit{string} ``bernardo310790'' \'{e} composta pelos \textit{tokens} ``bernardo'' e ``310790''.

\subsection{Modelo Cr\'{i}tico}

\par Prop\~{o}e-se a cria\c{c}\~{a}o do termo \textbf{modelo cr\'{i}tico}, definido como: seja $M(\tilde s)$ o conjunto de todos os modelos (elementares e compostos) cujos dom\'{i}nios possuem $\tilde s$ como membro.

\begin{center}
$M(\tilde{ s}) = \{\tilde{m}_{i=1}, \cdots, \tilde{m}_{N}\} | \forall \tilde\lambda_i, \tilde{ s} \in \tilde \lambda_i$
\end{center}

\par Seja $\hat m$ o modelo com menor cardinalidade em $M(\tilde s)$. Chama-se $\hat m(\tilde s)$ de \textbf{modelo cr\'{i}tico} de $\tilde s$, dado por:

\begin{equation}
\hat m(\tilde{s}) = \underset{\tilde m}{\arg\min} \ \mathcal{C}(\tilde m_i), \forall \tilde m_i \in M(\tilde s)
\end{equation}

\par Seja $\mathcal{M}_{\Gamma}$ o \textbf{multiconjunto de modelos cr\'{i}ticos} capazes de gerar cada \textit{string} no multiconjunto de amostras $\Gamma$, dado por:

\begin{equation}
\Gamma = \{ \tilde s_{j=1}, \cdots, \tilde s_L\} \implies \mathcal{M}_{\Gamma} = \{\hat m(\tilde s_{j=1}), \cdots, \hat m(\tilde s_L)\}
\end{equation}

\par Utiliza-se $\hat m(\tilde s_j)$ e $\hat m_j$ com o mesmo significado, denotando o dom\'{i}nio do modelo cr\'{i}tico $\hat m_j$ por $\hat \lambda_j$.

\subsection{Frequ\^{e}ncia Relativa}

\par Como $\mathcal{M}_{\Gamma}$ \'{e} multiconjunto, podem ocorrer membros $\hat m_i$ repetidos. Seja $n_i$ o n\'{u}mero de ocorr\^{e}ncias de cada $\hat m_i$ em $\mathcal{M}_{\Gamma}$, e seja $|\mathcal{M}_{\Gamma}|$ o n\'{u}mero total de membros de $\mathcal{M}_{\Gamma}$, incluindo repeti\c{c}\~{o}es. Define-se ent\~{a}o $\theta_i$ como a \textbf{frequ\^{e}ncia relativa} de $\hat m_i$ em $\mathcal{M}_{\Gamma}$, dado por:

\begin{equation}
 \theta(\tilde m_i) = \dfrac{n_i}{|\mathcal{M}_{\Gamma}|}
\end{equation}

\subsection{Entropia de Modelo}

\par A entropia pode ser definida com diversas bases para o logaritmo. Neste trabalho \'{e} utilizado o logaritmo de base $10$ em fun\c{c}\~{a}o da ordem de grandeza dos dados. Portanto, como $X \in \chi = \{x_{i=1}, \dots, x_N\}$, tem-se:

\begin{equation}
H(X) = \sum_{i}^{N}H(x_i) = - \sum_{i}^{N} P(x_i)\log_{10}P(x_i)
\end{equation}

\noindent onde $H(X)$ \'{e} a entropia da vari\'{a}vel aleat\'{o}ria discreta $X$. Neste trabalho, a entropia do modelo elementar $m(s)$ \'{e} dada por:

\begin{equation}
H(m(s)) = -\sum_{i=1}^{N} P(s_i)\log_{10}P(s_i)
\end{equation}

onde $s_i \in \lambda = \{ s_{i=1}, \dots, s_N\}$, ou seja, o somat\'{o}rio leva em conta todos as probabilidades de ocorr\^{e}ncia de cada \textit{string} (\textit{token}) contida no dom\'{i}nio do modelo elementar. No caso do modelo composto cr\'{i}tico $\hat m(\tilde s)$, a entropia \'{e} definida como o somat\'{o}rio das entropias de cada modelo elementar contido no mesmo, dado por:

\begin{equation}
H(\hat m(\tilde s)) = H(m_a(s)) + H(m_b(s)) + H(m_c(s)) + \cdots
\end{equation}

\section{Corinda}

A arquitetura do software baseia-se na an\'{a}lise de espa\c{c}os amostrais de senhas. Cada espa\c{c}o amostral consiste de arquivo no formato \textit{comma-separated value} (CSV), compactado no formato GZIP, que \'{e} do g\^{e}nero compactador de arquivos, que gera representa\c{c}\~{a}o eficiente de v\'{a}rios arquivos dentro de \'{u}nico arquivo, ocupando menos espa\c{c}o em m\'{i}dia. Cada entrada do arquivo consiste de uma senha, acompanhada pela respectiva frequ\^{e}ncia de ocorr\^{e}ncia. As entradas s\~{a}o ordenadas de forma decrescente de frequ\^{e}ncia. Os arquivos consistem das listas \textit{RockYou}, \textit{LinkedIn}, e \textit{AntiPublic}.

A an\'{a}lise dos conjuntos amostrais tem como objetivo encontrar os modelos cr\'{i}ticos correspondentes a cada senha da lista. Par\^{a}metros como entropia, complexidade, e frequ\^{e}ncia relativa dos modelos elementares e compostos s\~{a}o coletados no processo denominado \textbf{treinamento}.

Uma vez que a etapa de treinamento foi conclu\'{i}da, o \textbf{Corinda} pode ser utilizado para efetivamente \textbf{quebrar} \textit{hashes}. Para cada modelo composto detectado na etapa de treinamento, \'{e} lan\c{c}ada uma inst\^{a}ncia da gorrotina \mintinline{go}{composite.Model.Guess()}, respons\'{a}vel por gerar palpites de acordo com o modelo. A carga computacional alocada a cada gorrotina \'{e} proporcional \`{a} for\c{c}a do respectivo modelo.

O \textbf{Corinda} \'{e} implementado na forma de lista de arquivos:

\begin{enumerate}[label=\arabic*)]
\item \textbf{\mintinline{go}{corinda/elementary/elementary.go}}: contendo o pacote de modelo elementar.
\item \textbf{\mintinline{go}{corinda/composite/composite.go}}: contendo o pacote de modelo composto.
\item \textbf{\mintinline{go}{corinda/train/train.go}}: contendo o pacote de treinamento.
\item \textbf{\mintinline{go}{corinda/crack/crack.go}}: contendo o pacote de quebra de senhas.
\end{enumerate}

A intera\c{c}\~{a}o com o usu\'{a}rio no \textbf{Corinda} acontece com aux\'{i}lio da biblioteca Cobra, que \'{e} biblioteca utilizada em linguagem \textbf{Go} para a cria\c{c}\~{a}o de interface de comandos via \textit{console}. Cada arquivo sob o diret\'{o}rio \mintinline{go}{corinda/cmd/} representa o respectivo comando na interface de usu\'{a}rio.

\begin{enumerate}[label=\alph*)]
    \item \textbf{\mintinline{go}{corinda/cmd/train.go}}: treina modelos estat\'{i}sticos a partir de conjuntos amostrais.
    \item \textbf{\mintinline{go}{corinda/cmd/crack.go}}: usa modelos treinados para quebrar listas de \textit{hashes}.
    \item \textbf{\mintinline{go}{corinda/main.go}}: para execu\c{c}\~{a}o da gorrotina principal.
\end{enumerate}

\par Opta-se por subdividir a constru\c{c}\~{a}o do \textbf{Corinda} em Pacotes, facilitando o entendimento da metodologia proposta.

\section{Pacote Modelo Elementar}

\par Modelos elementares s\~{a}o implementados no pacote \mintinline{go}{elementary}, na forma da estrutura \mintinline{go}{elementary.Model}. O campo \mintinline{go}{elementary.Model.Name} representa o nome do modelo. O campo \mintinline{go}{elementary.Model.Entropy} representa a entropia do modelo, e o campo \mintinline{go}{elementary.Model.TokenFreqs} representa o mapa de frequ\^{e}ncias dos \textit{tokens} no espa\c{c}o amostral.

Os modelos elementares implementam os m\'{e}todos:

\begin{enumerate}
\item \texttt{elementary.Model.UpdateEntropy()}
\item \texttt{elementary.Model.UpdateTokenFreq()}
\item \texttt{elementary.Model.SortedTokens()}
\end{enumerate}

\subsection{M\'{e}todo elementary.Model.UpdateEntropy()}

\par Este m\'{e}todo \'{e} respons\'{a}vel por calcular a entropia do modelo elementar $m$. Primeiro, \'{e} calculado o somat\'{o}rio das frequ\^{e}ncias dos diferentes tokens $s_i$ no dom\'{i}nio \mintinline{go}{elementary.Model.TokenFreqs} do modelo. Ent\~{a}o, as frequ\^{e}ncias relativas $P(s_i)$ de todos elementos de \mintinline{go}{elementary.Model.TokenFreqs} s\~{a}o computadas e a entropia \mintinline{go}{elementary.Model.Entropy} \'{e} calculada como o somat\'{o}rio, dada por:

\begin{equation}
H(m) = - \sum P(s_i)\log_{10}P(s_i) 
\end{equation}

\subsection{M\'{e}todo elementary.Model.UpdateTokenFreq()}

\par Este m\'{e}todo \'{e} respons\'{a}vel por atualizar o campo \mintinline{go}{elementary.Model.TokenFreqs}. Caso o \textit{token} j\'{a} exista previamente no mapa, o valor da frequ\^{e}ncia \'{e} adicionado. Caso o \textit{token} ainda n\~{a}o exista, a nova entrada \'{e} adicionada, com a \textit{string} do \textit{token} agindo como chave e a frequ\^{e}ncia agindo como valor no mapa \mintinline{go}{elementary.Model.TokenFreqs}.

\subsection{M\'{e}todo elementary.Model.SortedTokens()}

\par Este m\'{e}todo \'{e} responsavel por retornar o \textit{array} de \textit{strings} com os \textit{tokens} do modelo elementar, organizados em ordem decrescente de frequ\^{e}ncia de ocorr\^{e}ncia no conjunto amostral.

\section{Pacote Modelo Composto}

Modelos compostos s\~{a}o implementados no pacote \mintinline{go}{composite}, na forma da estrutura \mintinline{go}{Model}. O campo \mintinline{go}{Name} representa o nome do modelo composto. O campo \mintinline{go}{Freq} representa a frequ\^{e}ncia de ocorr\^{e}ncia do modelo composto no espa\c{c}o amostral. O campo \mintinline{go}{Prob} representa a probabilidade de ocorr\^{e}ncia (frequ\^{e}ncia relativa) do modelo composto no espa\c{c}o amostral. O campo \mintinline{go}{Entropy} representa a entropia do modelo composto. O campo \mintinline{go}{Models} consiste do \textit{array} de \textit{strings} relativos aos nomes do modelos elementares que comp\~{o}em o modelo composto. 

Os modelos compostos implementam os m\'{e}todos:

\begin{enumerate}
\item \texttt{composite.Model.UpdateProb()}
\item \texttt{composite.Model.UpdateFreq()}
\item \texttt{composite.Model.UpdateEntropy()}
\item \texttt{composite.Model.recursive()}
\item \texttt{composite.Model.Guess()}
\item \texttt{composite.Model.digest()}
\end{enumerate}

\subsection{M\'{e}todo composite.Model.UpdateProb()}

\par Este m\'{e}todo \'{e} respons\'{a}vel por atualizar a frequ\^{e}ncia relativa $\theta(\hat m)$ do modelo composto, representada pelo objeto \mintinline{go}{cm.Prob}. O par\^{a}metro \mintinline{go}{cm.Freq} representa a frequ\^{e}ncia de ocorr\^{e}ncia do modelo composto cr\'{i}tico no conjunto amostral, enquanto o par\^{a}metro \mintinline{go}{freqSum} representa o somat\'{o}rio das frequ\^{e}ncias de todos os modelos compostos no conjunto amostral.

\subsection{M\'{e}todo composite.Model.UpdateFreq()}

\par O m\'{e}todo \mintinline{go}{composite.Model.UpdateFreq()} \'{e} respons\'{a}vel por atualizar o par\^{a}metro \mintinline{go}{composite.Model.Freq} do modelo composto.

\subsection{M\'{e}todo composite.Model.UpdateEntropy()}

\par O m\'{e}todo \mintinline{go}{composite.Model.UpdateEntropy()} \'{e} respons\'{a}vel por atualizar o par\^{a}metro \mintinline{go}{composite.Model.Entropy}. O mapa \mintinline{go}{elementaries} cont\'{e}m ponteiros para os modelos elementares que comp\~{o}em o modelo composto $\hat m$ em quest\~{a}o, referenciado por \mintinline{go}{cm}.

\par A entropia do modelo composto \'{e} estabelecida como o somat\'{o}rio das entropias dos modelos elementares que o comp\~{o}em.

\subsection{M\'{e}todo composite.Model.recursive()}

\par Este m\'{e}todo \'{e} respons\'{a}vel pela gera\c{c}\~{a}o recursiva do produto cartesiano $\tilde \lambda$ dos dom\'{i}nios dos modelos elementares. Por exemplo, sejam os conjuntos $\lambda_1$, $\lambda_2$ e $\lambda_3$ dos dom\'{i}nios dos modelos elementares que comp\~{o}em $\hat m$. O algoritmo recursivo produz o produto cartesiano $\tilde \lambda = \lambda_1 \times \lambda_2 \times \lambda_3$.

\subsection{M\'{e}todo composite.Model.Guess()}

O m\'{e}todo \mintinline{go}{composite.Model.Guess()} \'{e} respons\'{a}vel por gerar os palpites de senhas. Ele invoca o m\'{e}todo \mintinline{go}{composite.Model.recusive()} para enviar \textit{strings} de palpites pelo canal de sa\'{i}da. As \textit{strings} s\~{a}o compostas de acordo com o produto cartesiano dos dom\'{i}nios dos modelos elementares listados no \textit{array} \mintinline{go}{composite.Model.Models}.

\section{Pacote de Treinamento}

\par O pacote de treinamento fornece as rotinas necess\'{a}rias para an\'{a}lises dos conjuntos amostrais. Este pacote est\'{a} dividido em:

\begin{enumerate}
\item \texttt{train.Train.generator()}
\item \texttt{train.Train.batchAnalyzer()}
\item \texttt{train.Train.batchDecoder()}
\item \texttt{train.Train.mapsMerger()}
\item \texttt{train.Train.mapsSaver()}
\end{enumerate}

\par O arquivo de entrada \mintinline{go}{input.csv} cont\'{e}m a lista de pares ordenados \mintinline{go}{freq, password} no formato \textit{Comma Separated Value} (CSV). 

\subsection{Gorrotina train.Train.generator()}

\par A gorrotina \mintinline{go}{train.Train.generator()} \'{e} respons\'{a}vel por transformar cada linha do arquivo de entrada em objetos do tipo \mintinline{go}{input} e enviar vetores do tipo \mintinline{go}{inputBatch} pelo canal de sa\'{i}da. O n\'{u}mero de objetos \mintinline{go}{input} em cada vetor \mintinline{go}{inputBatch} \'{e} determinado pelo par\^{a}metro \mintinline{go}{batchSize}.

\subsection{Gorrotina train.Train.batchAnalyzer()}

\par A gorrotina \mintinline{go}{train.Train.batchAnalyzer()} \'{e} respons\'{a}vel por invocar a \textit{Java Virtual Machine} (JVM) com a \textit{thread} do \textit{Passfault}. Objetos do tipo \mintinline{go}{result} comp\~{o}em os vetores \mintinline{go}{resultBatch}, com o mesmo n\'{u}mero de elementos \mintinline{go}{batchSize}. Cada vetor \mintinline{go}{resultBatch} \'{e} enviado pelo canal \mintinline{go}{out}, enquanto a vari\'{a}vel $c$ indica o n\'{u}mero de senhas processadas no tempo. Cada elemento do tipo \mintinline{go}{result} cont\'{e}m a frequ\^{e}ncia de ocorr\^{e}ncia da senha, bem como as informa\c{c}\~{o}es de modelos elementares e cr\'{i}tico encontradas pelo \textit{Passfault}. O \textit{Passfault} organiza tais informa\c{c}\~{o}es no formato \textit{JavaScript Object Notation} (JSON) e as codifica na forma de \textit{arrays} de \textit{bytes}.

\par A gorrotina \mintinline{go}{batchDecoder()} \'{e} respons\'{a}vel por separar as informa\c{c}\~{o}es de cada objeto \mintinline{go}{result} em pares de objetos do tipo \mintinline{go}{elementaryJSON} e \mintinline{go}{compositeJSON}. As informa\c{c}\~{o}es contidas em cada par de objetos JSON \'{e} adicionada ao objeto \mintinline{go}{trainedMaps}, onde dois mapas s\~{a}o utilizados para referenciar os diferentes modelos elementares e compostos encontrados na senha. Para cada \mintinline{go}{resultBatch} recebida por \mintinline{go}{train.Train.batchDecoder()}, um objeto \mintinline{go}{trainedMaps} \'{e} eviado pelo canal \mintinline{go}{tmChan}. 

\subsection{Gorrotina train.Train.mapsMerger()}

\par Enquanto cada objeto \mintinline{go}{train.Train.trainedMaps} cont\'{e}m pares de mapas com informa\c{c}\~{o}es de \mintinline{go}{batchSize} senhas, o objeto \mintinline{go}{finalMaps} cont\'{e}m pares de mapas respons\'{a}vel por todas as senhas do conjunto amostral. A gorrotina \mintinline{go}{crack.Crack.mapsMerger()} \'{e} respons\'{a}vel por receber os diversos objetos \mintinline{go}{trainedMaps} e concaten\'{a}-los no par de mapas do objeto \mintinline{go}{finalMaps}. A vari\'{a}vel $m$ \'{e} utilizada para contabilizar o n\'{u}mero de mapas processados no tempo.

\par As opera\c{c}\~{o}es de uni\~{a}o dos diferentes mapas provenientes dos lotes de senhas possui alto custo computacional. De forma a evitar que a gorrotina \mintinline{go}{crack.Crack.mapsMerger()} seja gargalo no processamento de informa\c{c}\~{a}o, \'{e} importante que lotes inteiros de \mintinline{go}{batchSize} senhas sejam processados a cada itera\c{c}\~{a}o. Assim, a gorrotina \mintinline{go}{crack.Crack.mapsMerger()} est\'{a} sempre unindo mapas em lotes de \mintinline{go}{batchSize}.

\subsection{Gorrotina train.Train.mapsSaver()}

\par Por fim, a gorrotina \mintinline{go}{train.Train.mapsSaver()} \'{e} respons\'{a}vel por reorganizar as informa\c{c}\~{o}es dos mapas finais dos modelos elementares e compostos (\mintinline{go}{finalMaps}) e salv\'{a}-las no arquivo de sa\'{i}da \mintinline{go}{output.json}.

\par Antes de enviar os modelos elementares e compostos, a gorrotina invoca os m\'{e}todos \mintinline{go}{composite.Model.UpdateProb()}, \mintinline{go}{composite.Model.UpdateFreq()}, \mintinline{go}{composite.Model.UpdateEntropy()}, e \mintinline{go}{elementary.Model.UpdateEntropy()} para atualizar os par\^{a}metros internos dos modelos elementares e compostos encontrados durante o processo de treinamento.

\subsection{Fluxograma do Pacote de Treinamento}

\par A Figura \ref{fig:train1} apresenta o fluxograma do funcionamento do pacote de treinamento do \textbf{Corinda}. A Figura \ref{fig:trainl} disp\~{o}e as legendas dos objetos contidos no fluxograma. O arquivo \mintinline{go}{input.csv} \'{e} utilizado como entrada para a gorrotina \mintinline{go}{train.Train.generator()}. Objetos do tipo \mintinline{go}{input} s\~{a}o agrupados em objetos \mintinline{go}{inputBatch} (lote de entradas), e s\~{a}o enviados no canal de sa\'{i}da desta gorrotina. 

\par A gorrotina \mintinline{go}{train.Train.batchAnalyze()} \'{e} respons\'{a}vel por inicializar a JVM e o \textit{Passfault}. Os objetos \mintinline{go}{input} s\~{a}o recebidos e para cada entrada \'{e} gerado um objeto \mintinline{go}{result}. Objetos do tipo \mintinline{go}{result} s\~{a}o agrupados em \mintinline{go}{resultBatch} e enviados no canal de sa\'{i}da da gorrotina. A vari\'{a}vel $c$ computa o n\'{u}mero de entradas processadas pelo \textit{Passfault}. A gorrotina \mintinline{go}{train.Train.batchDecoder()} recebe os lotes de objetos \mintinline{go}{result} e os decodifica em objetos do tipo \mintinline{go}{elementaryJSON} e \mintinline{go}{compositeJSON}. Para cada lote \mintinline{go}{resultBatch} \'{e} criado o mapa \mintinline{go}{trainedMaps} contendo os modelos elementares e compostos identificados no lote. Os mapas \mintinline{go}{trainedMaps} s\~{a}o enviados no canal de sa\'{i}da.

\par A gorrotina \mintinline{go}{train.Train.mapsMerger()} \'{e} respons\'{a}vel por receber os diferentes \mintinline{go}{trainedMaps} relativos aos lotes de entradas e criar o \'{u}nico mapa contendo todos os modelos elementares e compostos do conjunto amostral. A vari\'{a}vel $m$ computa o n\'{u}mero de mapas processados. A gorrotina \mintinline{go}{train.Train.reporter()} \'{e} respons\'{a}vel por ler os ponteiros das vari\'{a}veis $c$ e $m$ e imprimir no \textit{console} o progresso do processo de treinamento. A gorrotina \mintinline{go}{train.Train.mapsSaver()} \'{e} respons\'{a}vel por receber o mapa final e salv\'{a}-lo no arquivo de sa\'{i}da \mintinline{go}{models.json}.

\begin{figure}[h!]
\centering
\includegraphics[width=230pt]{trainf}
\caption{Fluxograma do Processo de Treinamento.}
\label{fig:train1}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=430pt]{trainl}
\caption{Legendas do Fluxograma do Processo de Treinamento.}
\label{fig:trainl}
\end{figure}


\section{Pacote de Quebra de Senhas}

\par O pacote de quebra de senhas fornece o c\'{o}digo necess\'{a}rio para a implementa\c{c}\~{a}o da metodologia heur\'{i}stica de quebra de senhas. A gorrotina \mintinline{go}{crack.Crack()} gerencia os processos de quebra de senhas. O arquivo de entrada \mintinline{go}{models.json} cont\'{e}m os mapas com ponteiros para os modelos elementares e compostos encontrados no conjunto amostral durante o processo de treinamento.

\subsection{Gorrotina de gera\c{c}\~{a}o de palpites}

\par Para cada modelo composto do arquivo de entrada, \'{e} lan\c{c}ada a gorrotina \mintinline{go}{composite.Model.Guess()}, respons\'{a}vel por gerar palpites na forma de \textit{strings}. A cada gorrotina \'{e} atribu\'{i}do o par\^{a}metro \mintinline{go}{nGuesses}, descrito por:

\begin{equation}
\mintinline{go}{nGuesses}(\hat m_i) = \theta(\hat m_i) \cdot H(\hat m_i)
\end{equation}

\par O termo $\theta(\hat m_i)$ representa a frequ\^{e}ncia relativa do modelo composto $\hat m_i$ no conjunto amostral $\Gamma$, enquanto o termo $H(\hat m_i)$ representa a entropia de $\hat m_i$ em $\Gamma$. 

\par Esta modelagem iterativa permite que modelos com altos valores de $\theta(\hat m_i$)  componham maior parcela de palpites \mintinline{go}{guess} no lote \mintinline{go}{batch} de palpites. Da mesma forma, modelos com altas entropias $H(\hat m_i)$ tamb\'{e}m geram mais palpites por lote. Assim, a cada itera\c{c}\~{a}o, um \'{u}nico lote \'{e} processado, e a carga computacional efetivamente direcionada a cada modelo $\hat m_i$ \'{e} parametrizada pelo indicador \mintinline{go}{nGuesses(}$\hat m_i$\mintinline{go}{)}.

\par A organiza\c{c}\~{a}o das diversas gorrotinas geradoras de palpites \'{e} produzida de maneira iterativa. A cada itera\c{c}\~{a}o, cada modelo composto gera seus respectivos \mintinline{go}{nGuesses} palpites. Tal organiza\c{c}\~{a}o garante que a carga computacional alocada a cada modelo composto seja proporcional \`{a} entropia e frequ\^{e}ncia relativa do mesmo. Cada lote de palpites gerado ao fim de cada itera\c{c}\~{a}o \'{e} descrito pelo elemento \mintinline{go}{batch}. Cada lote possui total $T$ de palpites:

\begin{equation}\label{nguesses}
T = \sum_{i=0}^{N} \mintinline{go}{nGuesses}(\hat m_i) \\
\end{equation}

\par Assim, a carga computacional alocada a cada modelo $\hat m_i$ \'{e} dada por:

\begin{equation}
\mintinline{go}{CPUload}(\hat m_i) = \frac{ \mintinline{go}{nGuesses}(\hat m_i)}{T} 
\end{equation}

\subsection{Gorrotina crack.Crack.guessLoop()}

\par A gorrotina \mintinline{go}{crack.Crack.guessLoop()} \'{e} respons\'{a}vel por convergir o fluxo de palpites das diversas gorrotinas em canal \'{u}nico. A cada itera\c{c}\~{a}o do processo de quebra, o n\'{u}mero de objetos \mintinline{go}{password} gerados por cada modelo composto \'{e} condicionado pela entropia e frequ\^{e}ncia relativa do mesmo. 

\subsection{Gorrotina composite.Model.digest()}

\par A gorrotina \mintinline{go}{composite.Model.digest()} \'{e} respons\'{a}vel por transformar os diferentes palpites na forma de \textit{strings} para o tipo \mintinline{go}{password}. Tal processo consiste em computar a FDC (SHA1 ou SHA256) calculada a partir da \textit{string} e armazenar o \textit{hash} resultante na forma de \textit{array} de \textit{bytes}.

\subsection{Gorrotina crack.Crack.searcher()}
\par A gorrotina \mintinline{go}{crack.Crack.searcher()} \'{e} respons\'{a}vel por comparar os \textit{hashes} dos diferentes palpites gerados contra os \textit{hashes} alvo. Para cada palpite bem sucedido (palpite que produz \textit{hash} contido na lista de alvos), o respectivo objeto \mintinline{go}{password} \'{e} encaminhado para o canal de sa\'{i}da da gorrotina. Esta gorrotina atua como filtro, e o total de palpites bem sucedidos \'{e} igual ao n\'{u}mero \textit{t} de \textit{hashes} de palpites que foram encontrados na lista de \textit{hashes} alvo.

\subsection{Gorrotina crack.Crack.saver()}

\par A gorrotina \mintinline{go}{crack.Crack.saver()} \'{e} respons\'{a}vel por salvar os resultados das senhas efetivamente quebradas. Ela recebe os objetos \mintinline{go}{password} e os salva no arquivo \mintinline{go}{output.csv}. Os resultados s\~{a}o salvos na forma de pares ordenados \mintinline{go}{password, hash}, onde o \textit{hash} \'{e} representado no formato ASCII para visualiza\c{c}\~{a}o.

\subsection{Gorrotina crack.Crack.monitor()}

\par A gorrotina \mintinline{go}{monitor()} \'{e} respons\'{a}vel por monitorar o tempo decorrido desde o in\'{i}cio da sess\~{a}o. O objeto \mintinline{go}{wg} do tipo \mintinline{go}{sync.WaitGroup} atua como primitiva de sincroniza\c{c}\~{a}o. O valor em horas da dura\c{c}\~{a}o total do experimento \'{e} comparada com o limite \mintinline{go}{durationH}. Caso o limite de tempo seja excedido, o objeto \mintinline{go}{wg} \'{e} utilizado para finalizar todos os processos de quebra e sair do programa.

\subsection{Gorrotina crack.Crack.reporter()}

\par A gorrotina \mintinline{go}{crack.Crack.reporter()} \'{e} respons\'{a}vel por armazenar o progresso da sess\~{a}o no diret\'{o}rio \mintinline{go}{log_sessions}.

\subsection{Fluxograma do Pacote de Quebra de Senhas}

\par A Figura \ref{fig:crackf} apresenta o fluxograma do funcionamento do pacote de quebra de senhas do \textbf{Corinda}. A Figura \ref{fig:crackl} disp\~{o}e as legendas dos objetos contidos no fluxograma. O arquivo \mintinline{go}{models.json} \'{e} utilizado como entrada para a gorrotina \mintinline{go}{crack.Crack.Crack()}. A gorrotina carrega os diferentes objetos \mintinline{go}{composite.Model} referentes a cada modelo composto detectado no processo de treinamento. Cada objeto \mintinline{go}{composite.Model} \'{e} respons\'{a}vel por inicializar sua respectiva gorrotina \mintinline{go}{composite.Model.Guess()}, que gera \mintinline{go}{nGuesses(m)} palpites e os envia no canal de sa\'{i}da. Cada palpite \'{e} representado pelo objeto \mintinline{go}{guess}. 

\par A gorrotina \mintinline{go}{crack.Crack.guessLoop()} \'{e} respons\'{a}vel por receber os objetos \mintinline{go}{guess} dos diferentes modelos compostos e os enviar em \'{u}nico canal de sa\'{i}da. A cada itera\c{c}\~{a}o, s\~{a}o recebidos $T$ palpites para compor o lote, indicado pela linha tra\c{c}ejada no objeto \texttt{batch} da Figura \ref{fig:crackf}. A gorrotina \mintinline{go}{crack.Crack.digest()} \'{e} respons\'{a}vel por calcular o \textit{hash} de cada palpite recebido. Para cada objeto \mintinline{go}{guess} recebido do lote de palpites, a gorrotina encaminha um objeto \mintinline{go}{password} no canal de sa\'{i}da.

\par O objeto \mintinline{go}{targetsMap} cont\'{e}m o mapa dos \textit{hashes} alvo a serem quebrados. A gorrotina \mintinline{go}{crack.Crack.searcher()} \'{e} respons\'{a}vel por comparar os \textit{hashes} dos objetos \mintinline{go}{password} com os \textit{hashes} alvo de \mintinline{go}{targetsMap}. Para cada lote de palpites, a gorrotina encaminha os $t$ objetos \mintinline{go}{password} (correspondentes \`{a}s senhas quebradas) no canal de sa\'{i}da, onde $t \leq T$. A gorrotina \mintinline{go}{crack.Crack.saver()} \'{e} respons\'{a}vel por receber os objetos \mintinline{go}{password} correspondentes \`{a}s senhas quebradas no arquivo de sa\'{i}da \mintinline{go}{result.csv}.

\par A gorrotina \mintinline{go}{crack.Crack.monitor()} \'{e} respons\'{a}vel por monitorar o tempo de execu\c{c}\~{a}o do processo de quebra. Quando o crit\'{e}rio de parada tempo limite para o experimento \'{e} atingido, esta gorrotina interrompe o fluxo do programa. A gorrotina \mintinline{go}{crack.Crack.reporter()} \'{e} respons\'{a}vel por contabilizar o tempo decorrido desde o in\'{i}cio do experimento, bem como o n\'{u}mero total de senhas quebradas at\'{e} o momento. O progresso do experimento \'{e} impresso no \textit{console} e armazenado no diret\'{o}rio \mintinline{go}{/log_sessions}.

\begin{figure}[h!]
\centering
\includegraphics[width=300pt]{crackf}
\caption{Fluxograma do Processo de Quebra de Senhas.}
\label{fig:crackf}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=300pt]{crackl}
\caption{Legendas do Fluxograma do Processo de Quebra de Senhas.}
\label{fig:crackl}
\end{figure}

\section{Pacote da Interface de Comandos}

\par A biblioteca Cobra \'{e} utilizada para organizar e abstrair os comandos para o usu\'{a}rio. Aplica\c{c}\~{o}es baseadas na biblioteca Cobra possuem estruturas de comandos, argumentos, e \textit{flags}. Os comandos dispon\'{i}veis s\~{a}o \mintinline{go}{crack}, \mintinline{go}{help}, e \mintinline{go}{train}.

\subsection{Comando train}

\par O comando \texttt{train} \'{e} respons\'{a}vel por invocar o pacote \texttt{train/train.go}, executado por: \texttt{corinda train <conjunto amostral>}. O argumento \texttt{<conjunto amostral>} indica qual cojunto amostral deve ser utilizado no processo de treinamento (\texttt{rockyou}, \texttt{linkedin} ou \texttt{antipublic}). Por exemplo: \texttt{corinda train rockyou}.

\subsection{Comando crack}

\par O comando \texttt{crack} \'{e} respons\'{a}vel por invocar o pacote \texttt{crack/crack.go}, executado por: \texttt{corinda crack <conjunto amostral> <alvo> <fdc>} onde o argumento <conjunto amostral> indica o conjunto amostral a ser usado para gerar os palpites, e \mintinline{go}{<alvo>} indica a lista de \textit{hashes} alvo, e o argumento <fdc> indica o tipo de FDC a ser utilizada (\mintinline{go}{sha1} ou \mintinline{go}{sha256}). Ambos argumentos podem ser \mintinline{go}{rockyou}, \texttt{linkedin} ou \texttt{antipublic}. Por exemplo, para utilizar os modelos extra\'{i}dos da lista \texttt{rockyou} para quebrar os \textit{hashes} SHA1 da lista \texttt{linkedin}, utiliza-se o comando: \texttt{corinda crack rockyou linkedin sha1}.

\section{Experimentos e Valida\c{c}\~{a}o}

Os experimentos a serem realizados s\~{a}o dividos em duas categorias: i) experimentos preliminares e ii) valida\c{c}\~{a}o do \textbf{Corinda}. De forma a obter avalia\c{c}\~{a}o inicial dos dados sobre listas de senhas, utiliza-se o \textit{Passfault} como ferramenta de identifica\c{c}\~{a}o de modelos cr\'{i}ticos de senhas e suas cardinalidades. 
A Tabela \ref{table1} disp\~{o}e os poss\'{i}veis modelos elementares que o \textit{Passfault} \'{e} capaz de encontrar. 

Listas com palavras de sete diferentes idiomas s\~{a}o utilizadas como dicion\'{a}rios: alem\~{a}o, 
ingl\^{e}s, espanhol, italiano, holand\^{e}s e portugu\^{e}s. Sabe-se que idiomas seguem distribui\c{c}\~{o}es de Zipf, o que significa que a frequ\^{e}ncia de cada palavra \'{e} inversamente proporcional \`{a} sua posi\c{c}\~{a}o no ranking de frequ\^{e}ncias. Assim, para cada idioma, dois dicion\'{a}rios s\~{a}o utilizados: um com a \textbf{cabe\c{c}a} da distribui\c{c}\~{a}o (80\% mais frequentes), e outra com a 
\textbf{cauda longa} da distribui\c{c}\~{a}o.

\begin{table}[!htp]
    \centering
    \caption{Modelos elementares reconhecidos pelo \textit{Passfault}.}
    \label{table1}
    \tiny
      \begin{tabular}{|c|c|c|}
	\hline \textbf{Modelo Cr\'{i}tico} & \textbf{Descri\c{c}\~{a}o} & \textbf{Exemplo}\\
	\hline \pbox{4cm}{Correspond\^{e}ncia Exata em Dicion\'{a}rio}& \break \pbox{3cm}{.\\Sequ\^{e}ncia de caracteres encontrada no dicion\'{a}rio.\\} & \pbox{1cm}{\textit{john}}\\[2ex]
	\hline \pbox{4cm}{Correspond\^{e}ncia Invertida em Dicion\'{a}rio}& \pbox{3cm}{.\\Sequ\^{e}ncia de caracteres encontrada no dicion\'{a}rio, por\'{e}m com ordem invertida.\\} & \pbox{1cm}{\textit{nhoj}}\\[4ex]
	\hline \pbox{4cm}{Formato de Data} & \pbox{3cm}{.\\Sequ\^{e}ncia num\'{e}rica em formato de data.\\} & \pbox{1cm}{\textit{073190}}\\[2ex]
	\hline \pbox{4cm}{Sequ\^{e}ncia Aleat\'{o}ria de Caracteres} & \pbox{3cm}{.\\Sequ\^{e}ncia de caracteres sem padr\~{a}o reconhecido.\\} & \pbox{1cm}{\textit{a7mk0s}}\\[2ex]
	\hline \pbox{4cm}{Padr\~{o}es de Teclado} & \pbox{3cm}{.\\Sequ\^{e}ncia de caracteres obedecendo a Padr\~{o}es de disposi\c{c}\~{a}o espacial comumente encontrados em teclados.\\} & \pbox{1cm}{\textit{zxcvbnm}}\\[2ex]
	\hline \pbox{4cm}{Caracteres Repetidos} & \pbox{3cm}{.\\Sequ\^{e}ncia de caracteres repetidos.\\} & \pbox{1cm}{\textit{aaaa}}\\[2ex]
	\hline \pbox{4cm}{Repeti\c{c}\~{a}o de Sequ\^{e}ncia de Caracteres} & \pbox{3cm}{.\\Sequ\^{e}ncia de caracteres repete algum padr\~{a}o j\'{a} reconhecido.\\} & \pbox{1cm}{\textit{johnjohn}}\\[2ex]
        \hline
      \end{tabular}
     \vspace{4ex}
\end{table}

\subsection{Valida\c{c}\~{a}o do Corinda}

\par De forma a validar o desempenho do \textbf{Corinda}, s\~{a}o realizados experimentos que simulam cen\'{a}rios onde o atacante utiliza o conjunto de amostras $\Gamma$ para treinar o conjunto de modelos cr\'{i}ticos $\mathcal{M}_{\Gamma}$ com o objetivo de quebrar os \textit{hashes} pertencentes ao conjunto $F(\Gamma \rq)$.

\par S\~{a}o utilizados os conjuntos amostrais: i) \textit{RockYou}, ii) \textit{LinkedIn} e iii) \textit{AntiPublic}. Cada conjunto amostral consiste de arquivo no formato CSV, onde cada linha cont\'{e}m o valor \mintinline{go}{{frequencia, senha}}.

\par A natureza do algoritmo recursivo de gera\c{c}\~{a}o dos palpites possui a limita\c{c}\~{a}o de n\~{a}o priorizar \textit{tokens} de alta probabilidade. Assim, caso algum dos dom\'{i}nios dos modelos elementares em quest\~{a}o seja demasiadamente elevado, palpites com baixa probabilidade de sucesso podem ser gerados. De forma a investigar a influ\^{e}ncia do tamanho de $\Gamma$ na efic\'{a}cia de $\mathcal{M}_{\Gamma}$ para gerar os palpites durante o processo de quebra, para cada conjunto amostral, tamb\'{e}m s\~{a}o criadas vers\~{o}es truncadas, limitadas a 1 milh\~{a}o de entradas. Para cada lista, apenas o primeiro milh\~{a}o de senhas mais populares s\~{a}o utilizadas. Os conjuntos amostrais s\~{a}o referidos como: i) \textit{RockYou}\_1M, ii) \textit{LinkedIn}\_1M, e iii) \textit{AntiPublic}\_1M.

\par De forma a gerar as listas de \textit{hashes} alvo, os conjuntos amostrais (completos) s\~{a}o convertidos em \textit{hashes}, utilizando as FDC:

\begin{itemize}
 \item \textbf{SHA1}: o \textit{Secure Hash Algorithm 1} \'{e} a FDC estabelecida pela Ag\^{e}ncia de Seguran\c{c}a Nacional dos Estados Unidos (NSA) em 1993 \cite{dang_2013}. O SHA1 produz \textit{hash} de 20 \textit{bytes}. Estudos publicados desde 2005 comprovam a possibilidade de colis\~{a}o de \textit{hashes} SHA1, fazendo com que esta n\~{a}o seja mais considerada FDC segura \cite{wang2005collision}.

 \item \textbf{SHA256}: o SHA256 pertence \`{a} fam\'{i}lia de FDC conhecida como \textit{Secure Hash Algorithm 2}, tamb\'{e}m estabelecida pela NSA. Produz \textit{hash} de 256 bits. As fraquezas encontradas no SHA1 ainda n\~{a}o foram comprovadas no SHA256, fazendo com que este ainda seja considerado FDC segura \cite{glabb2007}, \cite{sanadhya_sarkar}.

\end{itemize}

\par A estrat\'{e}gia dos experimentos consiste em escolher o conjunto de amostras para treinar $\mathcal{M}_{\Gamma}$ e utiliz\'{a}-lo para quebrar os \textit{hashes} gerados a partir dos outros conjuntos. O mesmo processo \'{e} repetido para todas combina\c{c}\~{o}es poss\'{i}veis dos tr\^{e}s conjuntos \textit{RockYou}, \textit{LinkedIn} e \textit{AntiPublic} e suas vers\~{o}es truncadas, bem como das FDC: SHA1 e SHA256.

\section{Considera\c{c}\~{o}es}

\par Neste cap\'{i}tulo foi apresentada a metodologia para o desenvolvimento do software denominado \textbf{Corinda}. Os conceitos de modelo elementar e modelo composto cr\'{i}tico complementam a teoria dos modelos de primeira ordem de forma a descrever a estrutura das senhas. A arquitetura do \textbf{Corinda} \'{e} dividida em quatro pacotes: \mintinline{go}{elementary}, \mintinline{go}{composite}, \mintinline{go}{train} e \mintinline{go}{crack}. Al\'{e}m disto, a interface de comandos \'{e} implementada sob o diret\'{o}rio \mintinline{go}{corinda/cmd}. O pr\'{o}ximo cap\'{i}tulo apresenta os resultados obtidos decorrentes da metodologia proposta.
