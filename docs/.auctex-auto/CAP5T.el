(TeX-add-style-hook
 "CAP5T"
 (lambda ()
   (TeX-add-symbols
    '("specialcell" ["argument"] 1))
   (LaTeX-add-labels
    "chapter:cap5"
    "fig:52"
    "tab51"
    "tab:table3"
    "fig53"
    "Tab0"
    "Tab1"
    "fig:rockyou_fr"
    "rockyou_elementary_entropy"
    "rockyou_composite_entropy"
    "Tab2"
    "linkedin_elementary_probability"
    "linkedin_elementary_entropy"
    "linkedin_composite_entropy"
    "Tab3"
    "antipublic_composite_probability"
    "antipublic_elementary_entropy"
    "antipublic_composite_entropy"
    "sha1"
    "sha1trunc"
    "sha2"
    "sha2trunc"))
 :latex)

