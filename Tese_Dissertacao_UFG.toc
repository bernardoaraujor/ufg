\select@language {brazilian}
\thispagestyle {empty}
\cftpagenumbersoff {chapter}
\contentsline {chapter}{LISTA DE FIGURAS}{17}{section.0.1}
\cftpagenumberson {chapter}
\cftpagenumbersoff {chapter}
\contentsline {chapter}{LISTA DE TABELAS}{19}{section.0.2}
\cftpagenumberson {chapter}
\cftpagenumbersoff {chapter}
\contentsline {chapter}{LISTA DE S\'IMBOLOS}{21}{section.0.3}
\cftpagenumberson {chapter}
\cftpagenumbersoff {chapter}
\contentsline {chapter}{LISTA DE ABREVIATURAS E SIGLAS}{23}{section.0.4}
\cftpagenumberson {chapter}
\contentsline {chapter}{\numberline {1}INTRODU\c C\~AO}{25}{chapter.1}
\contentsline {chapter}{\numberline {2}QUEBRA DE SENHAS E LINGUAGEM GO}{29}{chapter.2}
\contentsline {section}{\numberline {2.1}Autentica\c c\~ao}{29}{section.2.1}
\contentsline {section}{\numberline {2.2}Quebra das Senhas}{29}{section.2.2}
\contentsline {section}{\numberline {2.3}Heur\IeC {\'\i }stica}{30}{section.2.3}
\contentsline {section}{\numberline {2.4}\textit {Passfault}}{31}{section.2.4}
\contentsline {section}{\numberline {2.5}Casos de Vazamentos}{31}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}\textit {RockYou}}{32}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}\textit {LinkedIn}}{32}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}\textit {AntiPublic}}{32}{subsection.2.5.3}
\contentsline {section}{\numberline {2.6}Linguagem de Programa\c c\~ao Go}{33}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Processos Sequenciais Comunicantes}{33}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Gorrotinas}{33}{subsection.2.6.2}
\contentsline {subsection}{\numberline {2.6.3}Canais}{34}{subsection.2.6.3}
\contentsline {subsection}{\numberline {2.6.4}\textit {Array}}{35}{subsection.2.6.4}
\contentsline {subsection}{\numberline {2.6.5}Mapa}{35}{subsection.2.6.5}
\contentsline {subsection}{\numberline {2.6.6}Estrutura, Tipo e Classe}{36}{subsection.2.6.6}
\contentsline {section}{\numberline {2.7}Considera\c c\~oes}{37}{section.2.7}
\contentsline {chapter}{\numberline {3}MODELAGEM DE SENHAS}{39}{chapter.3}
\contentsline {section}{\numberline {3.1}String}{39}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Conjunto Amostral}{40}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Opera\c c\~oes com Strings}{40}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Modelo}{41}{section.3.2}
\contentsline {section}{\numberline {3.3}Entropia}{42}{section.3.3}
\contentsline {section}{\numberline {3.4}Fun\c c\~ao de Dispers\~ao Criptogr\'afica}{42}{section.3.4}
\contentsline {section}{\numberline {3.5}Processo de Quebra}{43}{section.3.5}
\contentsline {section}{\numberline {3.6}Considera\c c\~oes}{44}{section.3.6}
\contentsline {chapter}{\numberline {4}METODOLOGIA}{45}{chapter.4}
\contentsline {section}{\numberline {4.1}Modelagem do Corinda}{45}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Modelo Elementar e Modelo Composto}{45}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}\textit {Token}}{47}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Modelo Cr\'{i}tico}{47}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Frequ\^{e}ncia Relativa}{48}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Entropia de Modelo}{48}{subsection.4.1.5}
\contentsline {section}{\numberline {4.2}Corinda}{48}{section.4.2}
\contentsline {section}{\numberline {4.3}Pacote Modelo Elementar}{50}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}M\'{e}todo elementary.Model.UpdateEntropy()}{50}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}M\'{e}todo elementary.Model.UpdateTokenFreq()}{50}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}M\'{e}todo elementary.Model.SortedTokens()}{51}{subsection.4.3.3}
\contentsline {section}{\numberline {4.4}Pacote Modelo Composto}{51}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}M\'{e}todo composite.Model.UpdateProb()}{51}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}M\'{e}todo composite.Model.UpdateFreq()}{51}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}M\'{e}todo composite.Model.UpdateEntropy()}{52}{subsection.4.4.3}
\contentsline {subsection}{\numberline {4.4.4}M\'{e}todo composite.Model.recursive()}{52}{subsection.4.4.4}
\contentsline {subsection}{\numberline {4.4.5}M\'{e}todo composite.Model.Guess()}{52}{subsection.4.4.5}
\contentsline {section}{\numberline {4.5}Pacote de Treinamento}{52}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Gorrotina train.Train.generator()}{53}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Gorrotina train.Train.batchAnalyzer()}{53}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}Gorrotina train.Train.mapsMerger()}{53}{subsection.4.5.3}
\contentsline {subsection}{\numberline {4.5.4}Gorrotina train.Train.mapsSaver()}{54}{subsection.4.5.4}
\contentsline {subsection}{\numberline {4.5.5}Fluxograma do Pacote de Treinamento}{54}{subsection.4.5.5}
\contentsline {section}{\numberline {4.6}Pacote de Quebra de Senhas}{55}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Gorrotina de gera\c {c}\~{a}o de palpites}{56}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}Gorrotina crack.Crack.guessLoop()}{57}{subsection.4.6.2}
\contentsline {subsection}{\numberline {4.6.3}Gorrotina composite.Model.digest()}{57}{subsection.4.6.3}
\contentsline {subsection}{\numberline {4.6.4}Gorrotina crack.Crack.searcher()}{58}{subsection.4.6.4}
\contentsline {subsection}{\numberline {4.6.5}Gorrotina crack.Crack.saver()}{58}{subsection.4.6.5}
\contentsline {subsection}{\numberline {4.6.6}Gorrotina crack.Crack.monitor()}{58}{subsection.4.6.6}
\contentsline {subsection}{\numberline {4.6.7}Gorrotina crack.Crack.reporter()}{58}{subsection.4.6.7}
\contentsline {subsection}{\numberline {4.6.8}Fluxograma do Pacote de Quebra de Senhas}{58}{subsection.4.6.8}
\contentsline {section}{\numberline {4.7}Pacote da Interface de Comandos}{59}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}Comando train}{59}{subsection.4.7.1}
\contentsline {subsection}{\numberline {4.7.2}Comando crack}{59}{subsection.4.7.2}
\contentsline {section}{\numberline {4.8}Experimentos e Valida\c {c}\~{a}o}{61}{section.4.8}
\contentsline {subsection}{\numberline {4.8.1}Valida\c {c}\~{a}o do Corinda}{61}{subsection.4.8.1}
\contentsline {section}{\numberline {4.9}Considera\c {c}\~{o}es}{63}{section.4.9}
\contentsline {chapter}{\numberline {5}RESULTADOS}{65}{chapter.5}
\contentsline {section}{\numberline {5.1}Experimentos Preliminares}{65}{section.5.1}
\contentsline {section}{\numberline {5.2}C\'odigo Fonte}{66}{section.5.2}
\contentsline {section}{\numberline {5.3}Valida\c c\~ao do Corinda}{70}{section.5.3}
\contentsline {section}{\numberline {5.4}Processos de treinamento}{71}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}\textit {RockYou}}{71}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}\textit {LinkedIn}}{72}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}\textit {AntiPublic}}{73}{subsection.5.4.3}
\contentsline {section}{\numberline {5.5}Quebra de Senhas}{74}{section.5.5}
\contentsline {section}{\numberline {5.6}Limita\c c\~oes do Corinda}{77}{section.5.6}
\contentsline {section}{\numberline {5.7}Coment\'arios}{78}{section.5.7}
\contentsline {chapter}{\numberline {6}CONCLUS\~AO}{79}{chapter.6}
\contentsline {section}{\numberline {6.1}Contribui\c c\~oes do Trabalho}{80}{section.6.1}
\contentsline {section}{\numberline {6.2}Sugest\~oes para Trabalhos Futuros}{81}{section.6.2}
\renewcommand {\chaptername }{\appendixname }
\contentsline {chapter}{\numberline {A}Pacote Modelo Elementar}{83}{appendix.A}
\contentsline {chapter}{\numberline {B}Pacote Modelo Composto}{87}{appendix.B}
\contentsline {chapter}{\numberline {C}Pacote de Treinamento}{91}{appendix.C}
\contentsline {chapter}{\numberline {D}Pacote de Quebra de Senhas}{103}{appendix.D}
\contentsline {chapter}{\numberline {E}Pacote de Interface de Comandos}{111}{appendix.E}
\contentsline {chapter}{\numberline {F}Exemplo de Produto Cartesiano}{115}{appendix.F}
\contentsline {chapter}{\numberline {G}Exemplo de Parametrizacao de Carga Computacional}{119}{appendix.G}
\contentsline {chapter}{REFER\^ENCIAS BIBLIOGR\'AFICAS}{125}{appendix.H}
\contentsline {chapter}{GLOSS\'ARIO}{129}{appendix.I}
\contentsfinish 
